﻿using UnityEngine;

public class Rebocoger : MonoBehaviour
{
    public float fuerzaSalto;
    private Rigidbody2D rb;
    public int cantidadManzanas;
    AudioSource aso;

    [Header("Tiempo!!!:")]
    public float timeout;
    private float timespent;

    public BarController barra;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        aso = GetComponent<AudioSource>();
        timespent = timeout;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("trampolin"))
        {
            rb.velocity = Vector2.zero;
            rb.AddForce(Vector2.up * fuerzaSalto);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("manzana"))
        {
            if (cantidadManzanas > 0)
            {
                cantidadManzanas--;
                Destroy(collision.gameObject);
                aso.pitch += 0.1f;
                aso.Play();
            }


        }
    }

    // Update is called once per frame
    void Update()
    {
        if (timespent > 0f)
        {
            timespent -= Time.deltaTime;
            barra.updateSlider(timespent, timeout);

            if (Input.GetAxis("Horizontal") < 0)
            {
                rb.AddForce(Vector2.left * fuerzaSalto / 50);
            }
            if (Input.GetAxis("Horizontal") > 0)
            {
                rb.AddForce(Vector2.right * fuerzaSalto / 50);
            }
            if (cantidadManzanas == 0)
            {
                Debug.Log("Gana");
            }
        }
    }
}
