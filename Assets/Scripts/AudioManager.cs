﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public AudioClip musicaIntro;
    public AudioClip musicaBattle;
    public AudioClip musicaDialog;

    private AudioSource audios;

    // Start is called before the first frame update
    void Start()
    {
        audios = GetComponent<AudioSource>();
    }

    public void musicChange(string song)
    {
        Debug.Log("Music Changed to " + song);
        if (song.Equals("1Splash"))
            audios.clip = musicaIntro;
        else if (song.Equals("3Story"))
            audios.clip = musicaDialog;
        else
            return;
        audios.Play();
    }
}
