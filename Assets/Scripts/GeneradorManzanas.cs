﻿using UnityEngine;

public class GeneradorManzanas : MonoBehaviour
{
    public float xmin;
    public float xmax;
    public float ymin;
    public float ymax;

    public int cuantasManzanas;

    public GameObject manzana;

    // Start is called before the first frame update
    void Start()
    {
        generarManzanas(cuantasManzanas);
    }

    private void generarManzanas(int cuantasManzanas)
    {
        for(int i = 0; i < cuantasManzanas; i++)
        {
            var xpos = Random.Range(xmin,xmax);
            var ypos = Random.Range(ymin, ymax);
            GameObject objeto = Instantiate(manzana, new Vector3(xpos, ypos, 0f), Quaternion.identity);
            objeto.transform.SetParent(null);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
