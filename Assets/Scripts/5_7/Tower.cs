﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public GameObject gm;
    public GameObject castPoint;

    private List<GameObject> apples;

    private int indexGlobal;

    [Range(0, 100)]
    public float forceImpulse;

    // Start is called before the first frame update
    void Start()
    {
        indexGlobal = 0;

        apples = new List<GameObject>();
        for (int i = 0; i < 20; i++)
        {
            GameObject newAppple = Instantiate(gm);
            newAppple.transform.position = new Vector3(1, 1, 1) * 100;
            apples.Add(newAppple);
        }

        InvokeRepeating("DropApple", 3, 4f);
    }

    void DropApple()
    {
        if (indexGlobal < apples.Count)
        {

            apples[indexGlobal].transform.position = castPoint.transform.position;
            apples[indexGlobal].transform.rotation = castPoint.transform.rotation;



            calculateAngle();
            calculateForce();

        }
        else
        {
            indexGlobal = 0;
        }
        indexGlobal++;
    }


    void calculateForce()
    {

        apples[indexGlobal].GetComponent<Rigidbody2D>().velocity = apples[indexGlobal].transform.right * Random.Range(5, forceImpulse);
    }

    void calculateAngle()
    {
        apples[indexGlobal].transform.rotation = Quaternion.Euler(0, 0, Random.Range(0, 70));
    }

}
