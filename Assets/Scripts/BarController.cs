﻿using UnityEngine;
using UnityEngine.UI;

public class BarController : MonoBehaviour
{

    public Slider slider;

    public void updateSlider(float amount, float total)
    {
        slider.value = amount / total;

    }
}
