﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    public float velocidad;

    private Transform trans;

    public bool puedo;

    // Start is called before the first frame update
    void Start()
    {
        trans = GetComponent<Transform>();
        puedo = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(puedo)
            transform.Rotate(new Vector3(0f, 0f, velocidad));
    }
}
