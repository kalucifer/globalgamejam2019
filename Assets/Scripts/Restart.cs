﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour {


	void Update () {
        if (Input.GetKeyDown(KeyCode.R)){    //con la tecla r se reinicia el juego
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}
}
