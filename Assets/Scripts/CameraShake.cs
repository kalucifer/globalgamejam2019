using System.Collections;
using UnityEngine;

/*

Este Script ha sido elaborado o modificado por Academia Applícate.

Licencia Apache V 2.0.
https://www.apache.org/licenses/LICENSE-2.0

Se prohibe su reproducción total o parcial sin el cumplimiento de los términos de licencia.
Copyright 2017 - Applícate Academia de Artes Digitales.

*/

public class CameraShake : MonoBehaviour {

	public Camera mainCam;

	float shakeAmount = 0;

	void Awake () {
		if (mainCam == null)
			mainCam = Camera.main;
	}

	public void Shake (float amt, float length) {
		shakeAmount = amt;
		InvokeRepeating ("DoShake", 0, 0.01f);
		Invoke ("StopShake", length);
	}

	void DoShake () {
		if (shakeAmount > 0) {
			Vector3 camPos = mainCam.transform.position;

			float offsetX = Random.value * shakeAmount * 2 - shakeAmount;
			float offsetY = Random.value * shakeAmount * 2 - shakeAmount;
			camPos.x += offsetX;
			camPos.y += offsetY;

			mainCam.transform.position = camPos;
		}
	}

	void StopShake () {
		CancelInvoke ("DoShake");
		mainCam.transform.position = new Vector3 (0, 0, -10);
	}
}