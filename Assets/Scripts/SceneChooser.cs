﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChooser : MonoBehaviour
{
    int index;

    [Header("Escenas de minijuegos de este personaje:")]
    public string[] escenas;

    [Header("Tiempo para pasar a la escena del minijuego:")]
    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        index = Random.Range(0, escenas.Length);
        StartCoroutine(SceneChange());
    }

    private IEnumerator SceneChange()
    {
        yield return new WaitForSeconds(timer);
        SceneManager.LoadScene(escenas[index]);
    }
}
