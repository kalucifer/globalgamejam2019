﻿using UnityEngine;

public class HuevoBehaviour : MonoBehaviour
{
    GameManager gm;
    Rigidbody2D rb;
    bool canWin;
    bool canLose;
    Spin spin;
    AudioSource audios;

    // Start is called before the first frame update
    void Start()
    {
        LeanTween.alpha(gameObject, 1f, 0.5f);
        //gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        rb = GetComponent<Rigidbody2D>();
        rb.isKinematic = true;
        canWin = true;
        canLose = true;
        spin = GetComponent<Spin>();
        audios = GetComponent<AudioSource>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("volcan") && canWin)
        {
            //gm.GameLoop();
            Debug.Log("Huevo encestado!!");
            LeanTween.scale(gameObject,new Vector3(0f,0f,0f),0.5f);
            canLose = false;
            spin.puedo = false;
            gameObject.SendMessage("NoMasHuevos");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.tag.Equals("huevo") && canLose)
        {
            Debug.Log("Huevo desperdiciado!!");
            LeanTween.alpha(gameObject, 0f, 0.5f);
            Destroy(gameObject, 2);
            canWin = false;
            spin.puedo = false;
            gameObject.SendMessage("NoMasHuevos");
            audios.Play();
        }
    }
}
