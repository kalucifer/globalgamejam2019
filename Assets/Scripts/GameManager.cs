﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    [Header("Escenas de personajes base:")]
    public List<string> escenas;

    [Header("Vidas iniciales de los personajes:")]
    public int initLives;

    private string scene;
    private AudioManager audioManager;

    private bool gameStarted = false;
    public bool GameStarted
    {
        get
        {
            return gameStarted;
        }

        set
        {
            gameStarted = value;
        }
    }

    private int dadLives;
    public int DadLives
    {
        get
        {
            return dadLives;
        }

        set
        {
            dadLives--;
        }
    }

    private int momLives;
    public int MomLives
    {
        get
        {
            return momLives;
        }

        set
        {
            momLives--;
        }
    }

    private int boyLives;
    public int BoyLives
    {
        get
        {
            return boyLives;
        }

        set
        {
            boyLives--;
        }
    }

    private int girlLives;
    public int GirlLives
    {
        get
        {
            return girlLives;
        }

        set
        {
            girlLives--;
        }
    }
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        scene = SceneManager.GetActiveScene().name;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Start()
    {
        audioManager = GetComponent<AudioManager>();
        dadLives = momLives = BoyLives = girlLives = initLives;
    }

    void Update()
    {

    }

    public void GameInit()
    {
        int index = Random.Range(0,escenas.Count);
        SceneManager.LoadScene(escenas[index]);
    }

    public void GameLoop()
    {

        if (dadLives < 1)
        {
            escenas.RemoveAt(0);
        }
        if (momLives < 1)
        {
            escenas.RemoveAt(1);
        }
        if (boyLives < 1)
        {
            escenas.RemoveAt(2);
        }
        if (girlLives < 1)
        {
            escenas.RemoveAt(3);
        }
        int index = Random.Range(0, escenas.Count);
        SceneManager.LoadScene(escenas[index]);
    }

    public void GameEnd()
    {
        SceneManager.LoadScene("5Ending");
    }

    void OnSceneLoaded(Scene _scene, LoadSceneMode mode)
    {
        scene = _scene.name;
        Debug.Log("Escena Cargada: " + _scene.name);
        audioManager.musicChange(_scene.name);  
    }
}
