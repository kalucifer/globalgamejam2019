﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LanzaHuevos : MonoBehaviour
{
    [Range(50f,100f)]
    [Header("La fuerza limite con la que se disparará:")]
    [Tooltip("Poner aquí el valor numérico...")]
    public float dureza;

    public GameObject huevo;
    private GameObject mano;
    private float fuerzaLanzamiento = 0;
    private GameObject nuevoHuevo;
    private Animator anim;

    [Header("Tiempo!!!:")]
    public float timeout;
    private float timespent;

    public GameObject flecha;
    public BarController barra;
    Spin spin;
    public bool puedoLanzar;

    // Start is called before the first frame update
    void Start()
    {
        mano = transform.GetChild(0).gameObject;
        anim = GetComponent<Animator>();
        timespent = timeout;
        flecha.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
        //flecha.SetActive(false);
        puedoLanzar = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (timespent > 0f && puedoLanzar)
        {
            timespent -= Time.deltaTime;
            barra.updateSlider(timespent, timeout);

            if (Input.GetButtonDown("Fire1"))
            {
                prepararHuevo();
            }
            if (Input.GetButton("Fire1"))
            {
                if (fuerzaLanzamiento < dureza)
                {
                    fuerzaLanzamiento++;
                    flecha.transform.localScale = new Vector3(fuerzaLanzamiento/150 + 0.4f, 0.4f, 1f);
                }
            }
            if (Input.GetButtonUp("Fire1"))
            {
                StartCoroutine(LanzarHuevo());
            }
        }
        else
        {
            Debug.Log("Perdido por tiempo");
            //gm.perder
        }
    }

    void NoMasHuevos()
    {
        puedoLanzar = false;
    }

    void prepararHuevo()
    {
        fuerzaLanzamiento = 0;
        nuevoHuevo = Instantiate(huevo, mano.transform.position, mano.transform.rotation);
        nuevoHuevo.transform.SetParent(null);
        Rigidbody2D rb = nuevoHuevo.GetComponent<Rigidbody2D>();
        rb.velocity = Vector2.zero;
        anim.SetBool("lanzando", false);
        flecha.SetActive(true);
        flecha.transform.localScale = new Vector3(0.1f,0.1f,1f);
        spin = nuevoHuevo.GetComponent<Spin>();
    }

    IEnumerator LanzarHuevo()
    {
        anim.SetBool("lanzando", true);
        Rigidbody2D rb = nuevoHuevo.GetComponent<Rigidbody2D>();
        yield return new WaitForSeconds(0.3f);
        spin.puedo = true;
        rb.isKinematic = false;
        rb.AddForce(new Vector2(-fuerzaLanzamiento * 5f, fuerzaLanzamiento * 6f));
        Debug.Log("Huevo lanzado con fuerza: " + fuerzaLanzamiento);
        flecha.SetActive(false);
    }
}
