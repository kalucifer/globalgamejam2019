﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    public int TimeLeft = 30;
    BarController Barra;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("LoseTime");
        Barra = GetComponent<BarController>();
        
    }

    // Update is called once per frame
    void Update()
    {

        if (TimeLeft <= 0)
        {
            StopCoroutine("LoseTime");

        }
        else
        {
            Barra.updateSlider(TimeLeft, 30);
        }
        
    }

    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            TimeLeft--;
            Debug.Log(TimeLeft);
        }
    }
}
