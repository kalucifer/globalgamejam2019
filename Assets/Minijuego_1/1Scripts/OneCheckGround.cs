using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneCheckGround : MonoBehaviour
{
    private OnePlayerController player1;
    // Start is called before the first frame update
    void Start()
    {
        player1 = GetComponentInParent<OnePlayerController>();
    }

    void OnCollisionStay2D(Collision2D col1)
    {
        if (col1.gameObject.tag == "Ground")
        {
            player1.grounded1 = true;
        }
    }

    void OnCollisionExit2D(Collision2D col1)
    {
        if (col1.gameObject.tag == "Ground")
        {
            player1.grounded1 = false;
        }
    }
}
