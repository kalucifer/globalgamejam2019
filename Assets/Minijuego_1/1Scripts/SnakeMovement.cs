using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeMovement : MonoBehaviour
{
    public float speed = 2f;

    private Rigidbody2D rb2d1;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
            transform.Translate(0, speed, 0);
    }
}
