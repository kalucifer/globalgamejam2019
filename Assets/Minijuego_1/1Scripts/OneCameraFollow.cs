using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneCameraFollow : MonoBehaviour
{
    public GameObject follow;
    public Vector2 minCamPos, maxCamPos;
    public float offSet = 3;

    void Start()
    {
      
    }

    void FixedUpdate()
    {
        float posY = follow.transform.position.y;
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(posY + offSet,minCamPos.y, maxCamPos.y), transform.position.z);

    }
}
