using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnePlayerController : MonoBehaviour
{
    public float speed1 = 2f;
    public float maxSpeed1 = 5f;
    public bool grounded1;
    public float jumpPower1 = 6.5f;

    private Rigidbody2D rb2d1;
    private SpriteRenderer mySpriteRenderer;
    private Animator anim;
    private bool jump1;
    // Start is called before the first frame update
    void Start()
    {
        rb2d1 = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("Speed1", Mathf.Abs(rb2d1.velocity.x));
        anim.SetBool("Grounded1", grounded1);

        if (Input.GetKeyDown(KeyCode.UpArrow) && grounded1)
        {
            jump1 = true;
        }
    }

    void FixedUpdate()
    {
        Vector3 fixedVelocity = rb2d1.velocity;
        fixedVelocity.x *= 0.75f;

        if(grounded1)
        {
            rb2d1.velocity = fixedVelocity;
        }

        float h1 = Input.GetAxis("Horizontal");
        rb2d1.AddForce(Vector2.right * speed1 * h1);

        if(rb2d1.velocity.x > maxSpeed1)
        {
            rb2d1.velocity = new Vector2(maxSpeed1, rb2d1.velocity.y);
        }

        if(rb2d1.velocity.x < -maxSpeed1)
        {
            rb2d1.velocity = new Vector2(-maxSpeed1, rb2d1.velocity.y);
        }

        if (jump1)
        {
            rb2d1.velocity = new Vector2(rb2d1.velocity.x, 0);
            rb2d1.AddForce(Vector2.up * jumpPower1, ForceMode2D.Impulse);
            jump1 = false;
        }

        Debug.Log(rb2d1.velocity.x);
    }
}

