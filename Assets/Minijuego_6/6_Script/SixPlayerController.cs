using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SixPlayerController : MonoBehaviour
{
    public float speed6 = 2f;
    public float maxSpeed6 = 5f;

    private Rigidbody2D rb2d6;
    // Start is called before the first frame update
    void Start()
    {
        rb2d6 = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        Vector3 fixedVelocity6 = rb2d6.velocity;
        fixedVelocity6.x *= 0.75f;
        rb2d6.velocity = fixedVelocity6;
        float h6 = Input.GetAxis("Horizontal");
        rb2d6.AddForce(Vector2.right * speed6 * h6);
        if (rb2d6.velocity.x > maxSpeed6)
        {
            rb2d6.velocity = new Vector2(maxSpeed6, rb2d6.velocity.y);
        }

        if (rb2d6.velocity.x < -maxSpeed6)
        {
            rb2d6.velocity = new Vector2(-maxSpeed6, rb2d6.velocity.y);
        }

        Debug.Log(rb2d6.velocity.x);
    }
}