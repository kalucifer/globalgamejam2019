using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class random : MonoBehaviour
{
    public GameObject[] prefab;

    public GameObject[] spawners;

    public GameObject hueso;
    public GameObject manzana;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnObjects());
    }

    // Update is called once per fram

    void OnBecameInvisible()
    {

    }

    IEnumerator spawnObjects()
    {
        int aleatorio = Random.Range(0, 4);
        float tiempo = Random.Range(0.5f,1.2f);

        for(int i=1; i<25;i++)
        { 
        yield return new WaitForSeconds(tiempo);

        int hm = Random.Range(0, 2);

        if (hm == 1)
        {
           // prefab = GameObject.FindGameObjectsWithTag("Manzana");
            GameObject objeto = Instantiate(manzana, spawners[aleatorio].transform.position, spawners[aleatorio].transform.rotation);
        }

            if (hm == 0)
            {
                prefab = GameObject.FindGameObjectsWithTag("Hueso");
                GameObject objeto = Instantiate(hueso, spawners[aleatorio].transform.position, spawners[aleatorio].transform.rotation);
            }

        }
    }

     void OnTriggerEnter(Collider2D col)
    {
        if(col.gameObject.tag=="Manzana")
        {
            //GANA
        }

        if(col.gameObject.tag =="Hueso")
        {
            //PIERDE
        }
    }
}
